# Prueba de Experiencia (FullStack)


## 1.-FRONTEND
* ReactJS
* JSX
* fetch Api consumer
* CoreUI React-Components
* CSS (sass, Bootstrap)


>## 1.1.-Requirements
 * npm/yarn package manager
 * nodejs

>## 1.2.-Deploy
>* git clone git@gitlab.com:oprada.java.developer/prueba-seleccion-fullstack.git
>* bash/powershell(projectRoot) : cd frontend 
>* npm install / yarn install
>* npm run start / yarn start

>## 1.3.-Environment
>* localhost port : 3000



## 2.-BACKEND
* NodeJs
* JS ES5/ES6
* ExpressJS
* Axios
* MongoDB
* Type : modules


>## 2.1.-Requirements
 * npm/yarn package manager
 * nodejs

>## 2.2.-Deploy
>* git clone git@gitlab.com:oprada.java.developer/prueba-seleccion-fullstack.git
>* bash/powershell(projectRoot) : cd backend 
>* npm install / yarn install
>* npm run start / yarn start

>## 2.3.-Environment
>>>> Cloud:
>>>>>>>* Heroku : https://tactechbackend.herokuapp.com/
>>>>>>>* MongoDB Atlas: mongodb+srv://oliver:<password>@cluster0.iuc2e.mongodb.net/Characters?retryWrites=true&w=majority

>>>> Local:
>>>>>>>* Localhost port: 4000


