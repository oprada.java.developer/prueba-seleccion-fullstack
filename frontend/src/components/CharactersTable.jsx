import React, { useEffect, useState } from 'react';
import {useHistory} from 'react-router-dom';
import {CCard,CDataTable,CCardBody,CButton} from '@coreui/react';

export const CharactersTable = () => {

  const [items, setItems] = useState([])
  const history = useHistory();

  useEffect(() => {
    fetch("http://localhost:4000/characters/")
      .then(response => response.json())
      .then(data => setItems(data));
  }, [])

  const fields = [
    { key: 'name', _style: { width: '20%'} },
    { key: 'house', _style: { width: '20%'} },
    {
      key: 'show_details',
      label: 'Actions',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ]
console.log()
  return (
    <CCard className="card-table">
      <CCardBody>
    <CDataTable
      items={items.data}
      fields={fields}
      tableFilter
      footer
      itemsPerPageSelect
      itemsPerPage={10}
      hover
      sorter
      pagination
      scopedSlots = {{

        'show_details':
          (item, index)=>{
            return (
              <td className="py-2">
                <CButton
                  color="primary"
                  variant="outline"
                  shape="square"
                  size="sm"
                  onClick={() => history.push("/view/"+item._id)}
                >
                  Details
                </CButton>
              </td>
              )
          }

      }}
    />
    </CCardBody>
</CCard>

  )
}
