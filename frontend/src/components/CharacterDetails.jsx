import React, { useEffect, useState } from 'react';
import { useParams,useHistory } from "react-router-dom";
export const CharacterDetails = () => {


const [character, setCharacter] = useState({});
const { id } = useParams()
const history = useHistory()

  useEffect(() => {
    fetch("http://localhost:4000/characters/" + id)
      .then(response => response.json())
      .then(data => setCharacter(data));
  }, [id])

  return (
    <>
    {character.data && (<>
      <div class="card card-details">
        <div class="card-header">
          Games Of Thrones
        </div>
        <div class="card-body">
          <h2 class="card-title">{character.data.name}</h2>
          <div className="box">
            <img src={character.data.image || "https://i.ibb.co/FJF4Thy/juego-de-tronos-s08e03-1.jpg"} alt="juego-de-tronos-s08e03-1" border="0" />
          </div>
          <h4 class="card-title">Slug: {character.data.slug}</h4>
          <h4 class="card-title">Title: {character.data.titles ? character.data.titles.join(", "):""}</h4>
          <h4 class="card-title">House: {character.data.house}</h4>
          <h4 class="card-title">Gender: {character.data.gender}</h4>
          <h4 class="card-title">Books: {character.data.books ? character.data.books.join(", "):""}</h4>
          <button onClick={() => history.goBack()} class="btn btn-primary">Go Back</button>
        </div>
      </div>
    </>)}

    </>
  )
}
