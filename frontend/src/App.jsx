import {CContainer} from '@coreui/react';
import { Layout } from "./layout/Layout";
export const App = ()=> {
  return (
    <CContainer className="container" fluid>
     <Layout/>
     </CContainer>
  );
}


