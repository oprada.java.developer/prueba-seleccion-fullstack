import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { List } from '../views/List';
import { View } from '../views/View';


export const Body = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/list' component={List}/>
        <Route exact path='/view/:id' component={View}/>
        <Redirect from="*" to='/list' />
      </Switch>
    </BrowserRouter>

  );
};
