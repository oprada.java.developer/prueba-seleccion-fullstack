import  pkg  from 'mongoose';
const {connect} = pkg;
import config from './config.js'

let dbURL = config.DB_URL;

const connectDB = async () => {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        keepAlive: 30000,
        connectTimeoutMS: 20000,
    }

    try{
        await connect(dbURL,options);
        console.log('Connection Established');

    }
    catch(err){
        console.log(err);
    }
}

export default connectDB;
