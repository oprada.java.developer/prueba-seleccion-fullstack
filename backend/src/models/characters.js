import pkg from 'mongoose';
const { Schema, model } = pkg;


const Characters = new Schema({

    image:{type: String },

    name:{type:String},

    gender:{type:String},

    slug:{type:String},

    pagerank: {type: Object },

    house:{type:String},

    books:{type: Array},

    titles:{type: Array},



});

export default model('characters',Characters)
