import Characters from '../models/characters.js';
import axios from 'axios';
import config from '../data/config.js';

export const getAllCharacters = async ( request, response) => {
    try{
        let characters = await Characters.find({})
        return response.status(200).json({ data:characters });
    }
    catch(error){
        console.log(error);
        return response.status(500).send({ error:error.message });
    }

}

export const getCharacterId = async ( req, response ) => {
    const { id } = req.params
    try{

        let character = await Characters.findById ( {_id : id} );

        if( !character ){
            return response.status(404).json({ data:`Character doesn't exist!` });
        }
        else{
            return response.status(200).json({ data:character });
        }

    }
    catch( error ){
        console.log( error );
        return response.status(500).send({ error:'El id del personaje debe ser de 12 caracteres' });
    }
}

export const getGotChars = () => {
    try{
      Characters.countDocuments().then(async (count) => {
            if(count == 0){
                let { data } = await axios({
                    url:config.ENDPOINT,
                    method:'GET'
                });
                let newCharacters = data.map((char) => new Characters(char));
                Characters.create(newCharacters);
              }
      });
    }
    catch(error){
        console.log(error);
    }
}
