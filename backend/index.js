import express from 'express';
import  connectDB  from './src/data/db.js';
import { getAllCharacters, getCharacterId } from './src/controllers/services.js';
import { getGotChars } from './src/controllers/services.js';
import cors from 'cors';
import config from './src/data/config.js';

const app = express();
const port = config.PORT ;
connectDB();
getGotChars();
app.use (cors({
  origin: '*'
}))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.get('/characters', getAllCharacters);
app.get('/characters/:id', getCharacterId);
app.get('/', (req, res, next) => {
res.json({"message":"Ok"})
});
app.listen( port, () => {
    console.log("server listening port: ", port);
})

export default app;
